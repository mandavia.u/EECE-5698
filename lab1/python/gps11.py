import sys
import time
import lcm
import serial
import utm
from exlcm import example_t

port = serial.Serial("/dev/ttyUSB0", baudrate = 4800)
lcm = lcm.LCM()
packet = example_t()
print ("GPS initializatin was done")

while True:
	line = port.readline()
	data = line.split(",")
       #print ("Hey")
       #print (data[0])
	if data[0] == "$GPGGA":
	       #print (data[1])
		utctime = float(data[1])
		packet.time = utctime
               
	       #print("Latitude: " + data[2])
		latgps = float(data[2])
		if data[3] == 'S':
			latgps = -latgps
                else:
                        latgps = latgps

		latdeg =int(latgps/100)
		latmin = latgps - latdeg*100
		lat = latdeg + (latmin/60)
		packet.latitude = float(lat)
                #print (packet.latitude)

		#print ("Longitude: " + data[4])
		longps = float(data[4])
		if data[5] == 'W':
			longps = -longps
		else:
			longps = longps

		londeg =int(longps/100)
		lonmin = longps - londeg*100
		lon = londeg + (lonmin/60)
		packet.longitude = float(lon)
                #print (packet.longitude)
                alt = float(data[9])
		packet.altitude = alt

		utmarray = utm.from_latlon(float(lat),float(lon))
		utm_x = utmarray[0]
		packet.utm_x = float(utmarray[0])
		#print (packet.utm_x)
                utm_y = utmarray[1]
                packet.utm_y = float(utmarray[1])
		#print (packet.utm_y)
                print (utctime,float(lat),float(lon),float(alt))
                print (utmarray)


                lcm.publish("GPS", packet.encode())

        
		


