import time
import socket
import sys
import utm
import rospy

from sensor_msgs.msg import NavSAtFix
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Points

def callback(data):
	rospy.loginfo(rospy.get_caller_id() + "I got %s", data.data)

def rtk_gps_node():
	rospy.init_node('rtk_gps_node',anonymus = True)
	rospy.Subscriber("rtk_fix",NavSatFix,callback)
	rospy.spin()

def gps2utm_node():

	pub = rospy.Publisher('/utm_fix', Odometry, queue_size=10)
	rospy.init_node('gps2utm_node')
	s = socket.socket()
	host = rospy.get_param('~host', '172.20.10.11')
	port = rospy.get_param('~port', 3000)
	s.connect((host,port))
	print ("Device is connected")


	gps_data = NavSatFix()
	utm_data = Odometry()
	xyz_data = Points()
	while not rospy.is_shutdown():
		GPS = s.recv(1024)
		print (GPS)

		try:
			data = GPS.split(",")
        	#print ("Hey")
        	#print (data[0])
			if data[0] == "$GPGGA":
	    	#print (data[1])
			time = float(data[1])
			#gps_data.time = time
               
	    	#print("Latitude: " + data[2])
			latgps = float(data[2])
			if data[3] == 'S':
				latgps = -latgps
        	else:
            	latgps = latgps

			latdeg =int(latgps/100)
			latmin = latgps - latdeg*100
			lat = latdeg + (latmin/60)
			#gps_data.latitude = float(lat)
        	#print (gps_data.latitude)

	    	#print ("Longitude: " + data[4])
			longps = float(data[4])
			if data[5] == 'E':
				longps = -longps
			else:
				longps = longps

			londeg =int(longps/100)
			lonmin = longps - londeg*100
			lon = londeg + (lonmin/60)
			#gps_data.longitude = float(lon)
        	#print (gps_data.longitude)
        	
        	alt = float(data[9])
			#gps_data.altitude = alt
        	#print(gps_data.altitude)
			

			utmarray = utm.from_latlon(float(lat),float(lon))
			utm_x = utmarray[0]
			utm_data.utm_x = float(utmarray[0])
			print (utm_data.utm_x)
        	
        	utm_y = utmarray[1]
        	utm_data.utm_y = float(utmarray[1])
			print (utm_data.utm_y)
			
			zone_number = utmarray[2]
			gps_data.zone_number = float(utmarray[2])
			print(gps_data.zone_number)

			zone_letter = utmarray[3]
			utm_data.zone_letter = str(utmarray[3])
			print(utm_data.zone_letter)

        	print (utctime,float(lat),float(lon),float(alt))
        	print (utmarray)
			
			x = utm_x
        	xyz_data.x = float(utm_x)
        	print(xyz_data.x)

        	y = utm_y
        	xyz_data.y = float(utm_y)
        	print(xyz_data.y)

        	z = alt
        	xyz_data.z = float(alt)
        	print(xyz_data.z)

        	pub.publish(utm_data)




if __name__ == '__main__':
	try:
		gps2utm_node()
	except rospy.ROSInterruptException:
		pass

