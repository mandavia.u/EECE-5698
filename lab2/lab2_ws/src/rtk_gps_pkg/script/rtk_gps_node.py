import time
import socket
import sys
import rospy
#import utm

from sensor_msgs import NavSatFix

def rtk_gps_node():

	pub = rospy.Publisher('/rtk_fix', NavSatFix, queue_size=10)
	rospy.init_node('rtk_gps_node')
	s = socket.socket()
	host = rospy.get_param('~host', '172.20.10.11')
	port = rospy.get_param('~port', 3000)
	s.connect((host,port))
	print ("Device is connected")


	gps_data = NavSatFix()
	while not rospy.is_shutdown():
		GPS = s.recv(1024)
		print (GPS)

		try:
			data = GPS.split(",")
        	#print ("Hey")
        	#print (data[0])
			if data[0] == "$GPGGA":
	    	#print (data[1])
			time = float(data[1])
			gps_data.time = time
               
	    	#print("Latitude: " + data[2])
			latgps = float(data[2])
			if data[3] == 'S':
				latgps = -latgps
        	else:
            	latgps = latgps

			latdeg =int(latgps/100)
			latmin = latgps - latdeg*100
			lat = latdeg + (latmin/60)
			gps_data.latitude = float(lat)
        	print (gps_data.latitude)

	    	#print ("Longitude: " + data[4])
			longps = float(data[4])
			if data[5] == 'E':
				longps = -longps
			else:
				longps = longps

			londeg =int(longps/100)
			lonmin = longps - londeg*100
			lon = londeg + (lonmin/60)
			gps_data.longitude = float(lon)
        	print (gps_data.longitude)
        	alt = float(data[9])
			gps_data.altitude = alt
        	print(gps_data.altitude)
			#utmarray = utm.from_latlon(float(lat),float(lon))
			#utm_x = utmarray[0]
			#gps_data.utm_x = float(utmarray[0])
			#print (packet.utm_x)
        	#utm_y = utmarray[1]
        	#gps_data.utm_y = float(utmarray[1])
			#print (packet.utm_y)
        	#print (utctime,float(lat),float(lon),float(alt))
        	#print (utmarray)


        	pub.publish(gps_data)

if __name__ == '__main__':
	try:
		rtk_gps_node()
	except rospy.ROSInterruptException:
		pass
		





                
