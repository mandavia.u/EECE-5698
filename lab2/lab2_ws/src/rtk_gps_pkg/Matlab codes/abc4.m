bagfile= rosbag('stationary10min_ISEC2.bag')
bagfile
bagfile.AvailableTopics

%odomBag=select(bagfile,'Topic','/utm_fix');
%odomBag
j = 1;
while true
    try
        odomBag=select(bagfile,'Topic','/utm_fix');
        odomBag
        odom = timeseries(odomBag,'Pose.Pose.Position.X','Pose.Pose.Position.Y','Pose.Pose.Position.Z')
         x11(j) = odom.Data(:,1)
         disp(x11)
         y11(j) = odom.Data(:,2)
         disp(y11)
        catch err
            break
    end
    j = j+ 1
end
x = odom.Data(:,1);
y = odom.Data(:,2);
mean_x = mean(odom.Data(:,1))
mean_y = mean(odom.Data(:,2))
std_x = std(x)
std_y = std(y)
xx = x - mean_x;
yy = y - mean_y;

alt = odom.Data(:,3);

plot(xx,yy,'+')
title('Stationary UTM Location ISEC building')
xlabel('UTM_X + 3.280*10^5 ')
ylabel('UTM_Y + 4.689*10^6')
%axis equal
%hold on
%plot(alt,'*')

legend({'gps position ISEC building'})

% hold on
% plot(x,'-')
% egend({'mean x'})
% hold on
% plot(y,'-')
% legend({'X position','mean x','mean y'})
% hold off

coavriance_x = 0.00909197;
covariance_y = 0.00235752;
DRMS_2 = 0.6;
CEP = 0.21576;

scatter3(xx,yy,alt)

