bagfile= rosbag('walking_ISEC5.bag')
bagfile
bagfile.AvailableTopics

%odomBag=select(bagfile,'Topic','/utm_fix');
%odomBag
j = 1;
while true
    try
        odomBag=select(bagfile,'Topic','/utm_fix');
        odomBag
        odom = timeseries(odomBag,'Pose.Pose.Position.X','Pose.Pose.Position.Y','Pose.Pose.Position.Z')
         x11(j) = odom.Data(:,1)
         disp(x11)
         y11(j) = odom.Data(:,2)
         disp(y11)
        catch err
            break
    end
    j = j+ 1
end
x = odom.Data(:,1);
y = odom.Data(:,2);
mean_x = mean(odom.Data(:,1))
mean_y = mean(odom.Data(:,2))
std_x = std(x)
std_y = std(y)
xx = x - mean_x;
yy = y - mean_y;

alt = odom.Data(:,3);

plot(xx,yy,'+')
title('Walking UTM Location ISEC building')
xlabel('UTM_X + 3.281*10^5 ')
ylabel('UTM_Y + 4.689*10^6')
%axis equal
%hold on
%plot(alt,'*')

legend({'walking gps position ISEC building'})

% hold on
% plot(x,'-')
% egend({'mean x'})
% hold on
% plot(y,'-')
% legend({'X position','mean x','mean y'})
% hold off

coavriance_x = 0.30076231;
covariance_y = 0.2837106;
DRMS_2 = 9.611689;
CEP = 4.008224;

scatter3(xx,yy,alt)
