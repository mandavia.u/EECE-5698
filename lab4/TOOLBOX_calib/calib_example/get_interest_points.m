% Local Feature Stencil Code
% CS 4495 / 6476: Computer Vision, Georgia Tech
% Written by James Hays

% Returns a set of interest points for the input image

% 'image' can be grayscale or color, your choice.
% 'feature_width', in pixels, is the local feature width. It might be
%   useful in this function in order to (a) suppress boundary interest
%   points (where a feature wouldn't fit entirely in the image, anyway)
%   or(b) scale the image filters being used. Or you can ignore it.

% 'x' and 'y' are nx1 vectors of x and y coordinates of interest points.
% 'confidence' is an nx1 vector indicating the strength of the interest
%   point. You might use this later or not.
% 'scale' and 'orientation' are nx1 vectors indicating the scale and
%   orientation of each interest point. These are OPTIONAL. By default you
%   do not need to make scale and orientation invariant local features.
function [x, y, confidence, scale, orientation] = get_interest_points(image, feature_width)

% Implement the Harris corner detector (See Szeliski 4.1.1) to start with.
% You can create additional interest point detector functions (e.g. MSER)
% for extra credit.

% If you're finding spurious interest point detections near the boundaries,
% it is safe to simply suppress the gradients / corners near the edges of
% the image.

% The lecture slides and textbook are a bit vague on how to do the
% non-maximum suppression once you've thresholded the cornerness score.
% You are free to experiment. Here are some helpful functions:
%  BWLABEL and the newer w will find connected components in 
% thresholded binary image. You could, for instance, take the maximum value
% within each component.
%  COLFILT can be used to run a max() operator on each sliding window. You
% could use this to ensure that every interest point is at a local maximum
% of cornerness.

image = imread('Image1.tif');
%image=downsample(image,4);
%J=downsample(J',4)';
%whos image J
%imshow(image)
%figure, imshow(image)
%image = double(image);
detectionType = 'harris'; %Set to 'harris' for Harris Feature Detection, 
                            %set to 'Fast' for Fast Detection

                            
%Harris Filter Values
threshold = 'min'; %set to -inf to disable thresholding, set to 'med' to set 
                    %threshold to the median value,'min' for min value or a number 
                    %to set threshold to that value;
noOfPts = 10000; %set to > 0 to find at most N best corner measures. Threshold will
                % increase from the minimum set until the number of points 
                %found is less than N
%Fast Filter Values
%threshold for filtering difference in target intensity over surrounding
%pixels.
fThreshold = 0.7; 
                
                
%For Adaptive Non-Maximal Suppression    
ANMSToggle = 0;%Set to 1 to enable Adaptive Non-maximal Suppresion, or 0 to remove it
ANMSNoOfPts = 5000;             
rParameter = 0.9; %Robustness Parameter

%get size of image
[rows cols] = size(image);


%%

%Harris Corner Detection

if strcmp(detectionType,'harris')

    %Gaussian Filters
    filterSize = 3;
    sigma1 = 1; %sigma of first Gaussian following recommendation by Szeliski (page 212)
    sigma2 = 1; %sigma of second Gaussian filter

    

    %Gaussian Filter

    %differentiation filter
    dx = [-1 0 1;
          -1 0 1;
          -1 0 1];

    dy = dx';

    g1 = fspecial('gaussian',filterSize,sigma1);

    %Gaussian Filter the image to remove noise
    imageG = conv2(image,g1,'same');

    %Image Derivatives 
    Ix = conv2(image,dx,'same');
    Iy = conv2(image,dy,'same');

    %Square of Derivatives
    Ix2_unfiltered = Ix.^2;
    Iy2_unfiltered = Iy.^2;
    Ixy_unfiltered = Ix.*Iy;

    %Gaussian Filter
    g2 = fspecial('gaussian',filterSize,sigma2);
    Ix2 = conv2(Ix2_unfiltered, g2, 'same');
    Iy2 = conv2(Iy2_unfiltered, g2, 'same');
    Ixy = conv2(Ixy_unfiltered, g2, 'same');

    %%Calculation of Cornerness value
    %Method: Using g(Ix2)g(Iy2) - (g(Ixy))^2 - a(g(Ix2)+g(Iy2))^2
    %where det = Ix2*Iy2-Ixy^2; trace = Ix2+Iy2;
    a = 0.04;
    har = Ix2.*Iy2 - Ixy.^2 - a*((Ix2 + Iy2).^2);
    
    
    %Corner Points Suppression

    minValue = min(min(har));

    %suppression of boundary spurrious points
%     har(1:floor(feature_width/2)-1,:) = minValue;
%     har(:,1:floor(feature_width/2)-1) = minValue;
%     har(rows-floor(feature_width/2)-1:rows,:) = minValue;
%     har(:,cols-floor(feature_width/2)-1:cols) = minValue;
    
    

    %thresholding
    maxValue = max(max(har));
    minValue = min(min(har));

    medianVal = median(median(har));

    %perform thresholding if is set to more than -inf
    if(threshold ~= -inf)
    %     threshold = medianVal;
    % harIndices = nx1 matrix of indices
        if threshold == 'med'
            threshold = medianVal;
        elseif threshold == 'min'
            threshold = minValue;
        end

        harIndices = find(har>threshold);

        %Loop to increase the threshold to find a number of best match points
        %that are lesser than a user-determined value, noOfPts.
        i = 0;
        if noOfPts>0
            while (size(harIndices,1)>noOfPts)
                i = i+1;
                threshold = threshold + (maxValue-threshold)/1000;
                harIndices = find(har>threshold);
            end
        end
        fprintf('No of times ran = %d\n',i);

        fprintf('threshold = %d, number of Features = %d\n',threshold, size(harIndices,1));
    end

elseif strcmp(detectionType,'fast')
    %Fast Detection algorithm
    %N = 12 based on first version of algorithm

    
    borderWidth = feature_width/2;
    
    har = zeros(rows,cols);
    
    for j = borderWidth:rows-borderWidth
        for i = borderWidth:cols-borderWidth
%             fprintf('j=%d;i=%d;\n',j,i);
            p0 = image(j,i);
            
                %intensity Threshold
            iThresh = image(j,i)*fThreshold;
            
            p1 = image(j-3,i); p5 = image(j,i+3); p9 = image(j+3,i);p13 = image(j,i-3);
            
            test1 = (p1>p0+iThresh)+(p5>p0+iThresh) + (p9>p0+iThresh) + (p13>p0+iThresh);
            
            test2 = (p1<p0-iThresh)+(p5<p0-iThresh) + (p9<p0-iThresh) + (p13<p0-iThresh);

            if ~(test1 >=3 ||test2>=3)
                continue;
            end
            
            p2 = image(j-3,i+1); p3 = image(j-2,i+2); p4 = image(j-1,i+3);
            p6 = image(j+1,i+3); p7 = image(j+2,i+2); p8 = image(j+3,i+1);
            p10 = image(j+3,i-1); p11 = image(j+2,i-2); p12 = image(j+1,i-3);
            p14 = image(j-1,i-3); p15 = image(j-2,i-2); p16 = image(j-3,i-1);
              
            
            test3 = (p1>p0+iThresh)+(p2>p0+iThresh) + (p3>p0+iThresh) + (p4>p0+iThresh)+...
                (p5>p0+iThresh)+(p6>p0+iThresh) + (p7>p0+iThresh) + (p8>p0+iThresh)+...
                (p9>p0+iThresh)+(p10>p0+iThresh) + (p11>p0+iThresh) + (p12>p0+iThresh)+...
                (p13>p0+iThresh)+(p14>p0+iThresh) + (p15>p0+iThresh) + (p16>p0+iThresh);

            test4 = (p1<p0-iThresh)+(p2<p0-iThresh) + (p3<p0-iThresh) + (p4<p0-iThresh)+...
                (p5<p0-iThresh)+(p6<p0-iThresh) + (p7<p0-iThresh) + (p8<p0-iThresh)+...
                (p9<p0-iThresh)+(p10<p0-iThresh) + (p11<p0-iThresh) + (p12<p0-iThresh)+...
                (p13<p0-iThresh)+(p14<p0-iThresh) + (p15<p0-iThresh) + (p16<p0-iThresh);
            
            if test3>=13 || test4>=13
                har(j,i) =1;
            end
        end
    end
    
    harIndices = find(har==1);
    
end

%%

tempSize = size(harIndices,1);
tempY = zeros(tempSize,1);
tempX = zeros(tempSize,1);

for i = 1:size(harIndices,1)
    tempY(i) = mod(harIndices(i),rows);

    if tempY(i) == 0
        tempX(i) = ceil(harIndices(i)/rows)-1;
        tempY(i) = rows;
    else
        tempX(i) = ceil(harIndices(i)/rows);
    end
end


%non-maximal suppression over a window of [3 3]
% if ANMSToggle == 0
%     for i = 1:tempSize
%         if har(tempY(i),tempX(i))~= real(max(har(tempY(i)-1:tempY(i)+1,tempX(i)-1:tempX(i)+1)))
%             tempY(i) = 0;
%             tempX(i) = 0;
%         end
%     end
% end

%Adaptive Non-Maximal Suppression (ANMS)
if ANMSToggle == 1
    
    nearestDist = zeros(tempSize, 1);
    
    %nx1 matrix
    harVal = har(harIndices);
    
    [harVal_sorted harVal_order] = sort(harVal,'descend');
    
    harIndices_sorted = harIndices(harVal_order,:);
    tempY_sorted = tempY(harVal_order,:);
    tempX_sorted = tempX(harVal_order,:);
    
    %Perform this for each feature
    for i = 1: tempSize
        %Get all distances between each feature with the others that have a
        %cornerness value significantly higher (determined by a robustness
        %parameter) than it.
        
        if (i == 1)
            nearestDist(1) = inf;
            continue;
        end
        for j = i-1:-1:1
            if har(harIndices_sorted(j))*rParameter > har(harIndices_sorted(i))
                dist(j) = sqrt((tempX_sorted(j) - tempX_sorted(i))^2 + (tempY_sorted(j) - tempY_sorted(i))^2);
            else
                dist(j) = inf;
            end
            
        end
        [nearestDist(i) pos] = min(dist);
    end
    
    %Gradually increase radius by skip amount until a desired number of
    %interest point is reached.
    radius = 0;
    radius_skip = 0.1;
    s = tempSize;
    ind = find(nearestDist>0);
    while(s>tempSize || s>ANMSNoOfPts)
        ind = find(nearestDist >= radius);
        s = size(ind,1);
        radius = radius + radius_skip;
    end
    fprintf('Radius = %d, Number of points = %d\n',radius-radius_skip,s);
    
    x(:,1) = tempX_sorted(ind);
    y(:,1) = tempY_sorted(ind);

end
%Set x and y values to be the Interest Points found
m = 1;
if ANMSToggle == 0
    for i = 1:tempSize 
        if tempY(i)<= 0 
            continue;
        end

        y(m,1) = tempY(i);
        x(m,1) = tempX(i);
        m = m+1;
    end
end

imshow(image);
hold on;
plot(x,y,'r.',1,20);
saveas(gcf,'intPts','jpg');
fprintf('Final Number of Interest Points = %d\n',size(y,1));
disp('end of get_interest_points'); 
end