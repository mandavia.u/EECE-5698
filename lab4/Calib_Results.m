% Intrinsic and Extrinsic Camera Parameters
%
% This script file can be directly executed under Matlab to recover the camera intrinsic and extrinsic parameters.
% IMPORTANT: This file contains neither the structure of the calibration objects nor the image coordinates of the calibration points.
%            All those complementary variables are saved in the complete matlab data file Calib_Results.mat.
% For more information regarding the calibration model visit http://www.vision.caltech.edu/bouguetj/calib_doc/


%-- Focal length:
fc = [ 19241.223265402648394 ; 52065.892946245825442 ];

%-- Principal point:
cc = [ 2034.268690293027021 ; -899.658903375050159 ];

%-- Skew coefficient:
alpha_c = 0.000000000000000;

%-- Distortion coefficients:
kc = [ -2.615045034798388 ; 202.031300905037739 ; 0.064202960430210 ; -0.061861247149029 ; 0.000000000000000 ];

%-- Focal length uncertainty:
fc_error = [ 414.781503261404680 ; 5569.999986649689163 ];

%-- Principal point uncertainty:
cc_error = [ 0.000000000000000 ; 0.000000000000000 ];

%-- Skew coefficient uncertainty:
alpha_c_error = 0.000000000000000;

%-- Distortion coefficients uncertainty:
kc_error = [ 0.629885872209415 ; 69.688208853622129 ; 0.016109721770870 ; 0.003714097530218 ; 0.000000000000000 ];

%-- Image size:
nx = 4032;
ny = 3024;


%-- Various other variables (may be ignored if you do not use the Matlab Calibration Toolbox):
%-- Those variables are used to control which intrinsic parameters should be optimized

n_ima = 6;						% Number of calibration images
est_fc = [ 1 ; 1 ];					% Estimation indicator of the two focal variables
est_aspect_ratio = 1;				% Estimation indicator of the aspect ratio fc(2)/fc(1)
center_optim = 0;					% Estimation indicator of the principal point
est_alpha = 0;						% Estimation indicator of the skew coefficient
est_dist = [ 1 ; 1 ; 1 ; 1 ; 0 ];	% Estimation indicator of the distortion coefficients


%-- Extrinsic parameters:
%-- The rotation (omc_kk) and the translation (Tc_kk) vectors for every calibration image and their uncertainties

%-- Image #1:
omc_1 = [ 1.886460e+00 ; 4.585610e-02 ; -6.195648e-02 ];
Tc_1  = [ -3.265357e+02 ; 5.580680e+02 ; 1.189573e+04 ];
omc_error_1 = [ 2.848063e-02 ; 8.043369e-04 ; 1.749182e-03 ];
Tc_error_1  = [ 6.362333e-01 ; 4.909628e+01 ; 2.589361e+02 ];

%-- Image #2:
omc_2 = [ 2.050245e+00 ; 5.670269e-02 ; -8.076889e-02 ];
Tc_2  = [ -3.461990e+02 ; 6.944645e+02 ; 1.103253e+04 ];
omc_error_2 = [ 4.586377e-02 ; 7.492795e-04 ; 2.851162e-03 ];
Tc_error_2  = [ 7.769007e-01 ; 6.122617e+01 ; 2.355714e+02 ];

%-- Image #3:
omc_3 = [ 2.142779e+00 ; 5.073390e-02 ; -8.980636e-02 ];
Tc_3  = [ -4.462171e+02 ; 7.782386e+02 ; 1.098525e+04 ];
omc_error_3 = [ 5.712960e-02 ; 9.446365e-04 ; 4.963361e-03 ];
Tc_error_3  = [ 9.082866e-01 ; 6.879064e+01 ; 2.302295e+02 ];

%-- Image #4:
omc_4 = [ 2.009697e+00 ; 5.867251e-02 ; -7.849717e-02 ];
Tc_4  = [ -4.749116e+02 ; 8.057063e+02 ; 1.131074e+04 ];
omc_error_4 = [ 4.114724e-02 ; 7.455863e-04 ; 2.308363e-03 ];
Tc_error_4  = [ 8.912855e-01 ; 7.090480e+01 ; 2.420406e+02 ];

%-- Image #5:
omc_5 = [ 2.048661e+00 ; -1.711893e-03 ; 9.445740e-04 ];
Tc_5  = [ -5.614660e+02 ; 7.830969e+02 ; 1.076355e+04 ];
omc_error_5 = [ 4.558032e-02 ; 7.195343e-04 ; 1.472696e-03 ];
Tc_error_5  = [ 8.807156e-01 ; 6.902237e+01 ; 2.292564e+02 ];

%-- Image #6:
omc_6 = [ 2.123337e+00 ; 5.461991e-02 ; -9.169698e-02 ];
Tc_6  = [ -5.110898e+02 ; 8.051625e+02 ; 1.096739e+04 ];
omc_error_6 = [ 5.462009e-02 ; 8.893659e-04 ; 4.687508e-03 ];
Tc_error_6  = [ 9.269864e-01 ; 7.109921e+01 ; 2.304397e+02 ];

