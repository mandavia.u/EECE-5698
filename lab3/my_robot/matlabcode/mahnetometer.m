
figure 
plot(utm_x,utm_y);
xlabel('utm_x')
ylabel('utm_y')
title('utm path')
figure
plot(mag_x,mag_y);
xlabel('mag_x')
ylabel('mag_y')
title('Raw magnetometer plot')

x=my_ellipse(mag_x,mag_y);
rotmatrix=[cos(-x.phi) sin(-x.phi);-sin(-x.phi) cos(-x.phi)];
hfull=[mag_x-x.X0_in; mag_y-x.Y0_in];
calib_matrixfulls=rotmatrix*hfull;
mag_x2=calib_matrixfulls(1,:);
mag_y2=calib_matrixfulls(2,:);
if x.a>x.b
    mag_x2=(x.b/x.a)*mag_x2;
elseif x.b>x.a
    mag_y2=(x.a/x.b)*mag_y2;
end
figure
plot(mag_x2,mag_y2)
grid on;
title('corected magnetometer readings for soft iron and hard iron effects');
xlabel('mag_x');
ylabel('mag_y');
figure
plot(mag_x,mag_y)
grid on;
title('raw magnetometer readings');
xlabel('mag_x');
ylabel('mag_y');

function ellipse_imu = my_ellipse( x,y,axis_handle )
orientation_tolerance = 1e-3;

warning( '' );

x = x(:);
y = y(:);

mean_x = mean(x);
mean_y = mean(y);
x = x-mean_x;
y = y-mean_y;

X = [x.^2, x.*y, y.^2, x, y ];
a = sum(X)/(X'*X);

if ~isempty( lastwarn )
    disp( 'stopped because of a warning regarding matrix inversion' );
    ellipse_imu = [];
    return
end

[a,b,c,d,e] = deal( a(1),a(2),a(3),a(4),a(5) );


if ( min(abs(b/a),abs(b/c)) > orientation_tolerance )
    
    orientation_rad = 1/2 * atan( b/(c-a) );
    cos_theta = cos( orientation_rad );
    sin_theta = sin( orientation_rad );
    [a,b,c,d,e] = deal(...
        a*cos_theta^2 - b*cos_theta*sin_theta + c*sin_theta^2,...
        0,...
        a*sin_theta^2 + b*cos_theta*sin_theta + c*cos_theta^2,...
        d*cos_theta - e*sin_theta,...
        d*sin_theta + e*cos_theta );
    [mean_x,mean_y] = deal( ...
        cos_theta*mean_x - sin_theta*mean_y,...
        sin_theta*mean_x + cos_theta*mean_y );
else
    orientation_rad = 0;
    cos_theta = cos( orientation_rad );
    sin_theta = sin( orientation_rad );
end


test = a*c;
switch (1)
case (test>0),  status = '';
case (test==0), status = 'Parabola found';  warning( 'fit_ellipse: Did not locate an ellipse' );
case (test<0),  status = 'Hyperbola found'; warning( 'fit_ellipse: Did not locate an ellipse' );
end


% if we found an ellipse return it's data
if (test>0)
    
    % make sure coefficients are positive as required
    if (a<0), [a,c,d,e] = deal( -a,-c,-d,-e ); end
    
    % final ellipse parameters
    X0          = mean_x - d/2/a;
    Y0          = mean_y - e/2/c;
    F           = 1 + (d^2)/(4*a) + (e^2)/(4*c);
    [a,b]       = deal( sqrt( F/a ),sqrt( F/c ) );    
    long_axis   = 2*max(a,b);
    short_axis  = 2*min(a,b);

    % rotate the axes backwards to find the center point of the original TILTED ellipse
    R           = [ cos_theta sin_theta; -sin_theta cos_theta ];
    P_in        = R * [X0;Y0];
    X0_in       = P_in(1);
    Y0_in       = P_in(2);
    
    % pack ellipse into a structure
    ellipse_imu = struct( ...
        'a',a,...
        'b',b,...
        'phi',orientation_rad,...
        'X0',X0,...
        'Y0',Y0,...
        'X0_in',X0_in,...
        'Y0_in',Y0_in,...
        'long_axis',long_axis,...
        'short_axis',short_axis,...
        'status','' );
else
    % report an empty structure
    ellipse_imu = struct( ...
        'a',[],...
        'b',[],...
        'phi',[],...
        'X0',[],...
        'Y0',[],...
        'X0_in',[],...
        'Y0_in',[],...
        'long_axis',[],...
        'short_axis',[],...
        'status',status );
end


% check if we need to plot an ellipse with it's axes.
if (nargin>2) & ~isempty( axis_handle ) & (test>0)
    
    % rotation matrix to rotate the axes with respect to an angle phi
    R = [ cos_theta sin_theta; -sin_theta cos_theta ];
    
    % the axes
    ver_line        = [ [X0 X0]; Y0+b*[-1 1] ];
    horz_line       = [ X0+a*[-1 1]; [Y0 Y0] ];
    new_ver_line    = R*ver_line;
    new_horz_line   = R*horz_line;
    
    % the ellipse
    theta_r         = linspace(0,2*pi);
    ellipse_x_r     = X0 + a*cos( theta_r );
    ellipse_y_r     = Y0 + b*sin( theta_r );
    rotated_ellipse = R * [ellipse_x_r;ellipse_y_r];
    
    % draw
    hold_state = get( axis_handle,'NextPlot' );
    set( axis_handle,'NextPlot','add' );
    plot( new_ver_line(1,:),new_ver_line(2,:),'r' );
    plot( new_horz_line(1,:),new_horz_line(2,:),'r' );
    plot( rotated_ellipse(1,:),rotated_ellipse(2,:),'r' );
    set( axis_handle,'NextPlot',hold_state );
end

end
