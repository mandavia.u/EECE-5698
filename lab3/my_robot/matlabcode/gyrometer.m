


mag_yaw_angle = atan2(mag_y,mag_x);
figure
plot(utime_imu,mag_yaw_angle)
title('mag_yaw_angle')
mag_yaw_anglew = unwrap(mag_yaw_angle)
figure 
plot(utime_imu,mag_yaw_anglew)
title('mag_yaw_angleunwrapped')
mag_yawangle_corrected=atan2(-mag_y2,mag_x2)
figure
plot(utime_imu,mag_yawangle_corrected)
title('mag_yawangle_corrected')
mag_yawangle_correctedw=unwrap(mag_yawangle_corrected)
figure
plot(utime_imu,mag_yawangle_correctedw)
title('mag_yawangle_correctedunwrapped')

figure 
grid on
plot(utime_imu,mag_yawangle_corrected)
hold on
plot(utime_imu,mag_yawangle_correctedw)
title('mag_yawangle_corrected regular vs unwrapped')
legend('mag_yawangle_corrected','mag_yawangle_correctedw')





gyroz_yaw_angle=cumtrapz(gyro_z);
figure 
plot(gyroz_yaw_angle)
title('INtegrated gyro z')
gyroz_wraptopi=wrapToPi(gyroz_yaw_angle);
figure
plot(gyroz_wraptopi)
title('gyroz_wraptopi')

yaw_combined= 0.98.*gyroz_yaw_angle+ 0.02.*mag_yawangle_corrected;
yaw_combined_wraptopi=wrapToPi(yaw_combined);
yaw_combined1= 0.98.*gyroz_wraptopi+ 0.02.*mag_yawangle_corrected;

figure
plot(utime_imu,yaw_combined);
grid on;
title('yaw_angle_combined');


figure
plot(utime_imu,yaw_combined_wraptopi);
grid on;
title('yaw_angle_combined_wrapped');

% figure 
% plot(mag_yawangle_correctedw,gyroz_wraptopi)
% title('calibratwd yaw angle vs gyro yaw angle wrap to pi')
