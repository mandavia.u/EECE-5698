% samplePeriod = 256
% filtCutOff = 0.001;
% [b, a] = butter(1, (2*filtCutOff)/(1/samplePeriod), 'high');
% acc_x_Filt = filtfilt(b, a, acc_x);
% 
% % Compute absolute value
% acc_x_Filt = abs(acc_x_Filt);
% 
% % LP filter accelerometer data
% filtCutOff = 5;
% [b, a] = butter(1, (2*filtCutOff)/(1/samplePeriod), 'low');
% acc_x_Filt = filtfilt(b, a, acc_x_Filt);

Fs = 1000;
fc = 50;
Wn = (2/Fs)*fc;
b = fir1(10,Wn,'low');
fvtool(b,1,'Fs',Fs);
acc_x_filt = filter(b,1,acc_x);
figure
plot(acc_x)
hold on
plot(acc_x_filt)
title('Low pass filter comparision for acc_x')
acc_y_filt = filter(b,1,acc_y)
figure
plot(acc_y)
hold on
plot(acc_y_filt)
title('Low pass filtering comparision for acc_y')

acc_mean_x=mean(acc_x_filt);
acc_mean_y=mean(acc_y_filt);

%acc_mean_z=mean(acc_z);

acc_mean_x1=acc_x_filt-acc_mean_x;
acc_mean_y1=acc_y_filt-acc_mean_y;
figure
plot(acc_x)
title('acc_x')

figure 
plot(acc_y)
title('acc_y')

figure
plot(acc_x_filt)
title('low pass filtered acc_x')

figure
plot(acc_y_filt)
title('low pass filtered acc_Y')
%acc_mean_z1=acc_z-acc_mean_z;

vel_x=cumtrapz(acc_mean_x1);
vel_mean_x = mean(vel_x);
vel_x1 = vel_x-vel_mean_x;
figure 
plot(vel_x)
title('vel_x1')

pos_x=cumtrapz(vel_x);
pos_mean_x = mean(pos_x)
pos_x1 = pos_x-pos_mean_x;



vel_y=cumtrapz(acc_mean_y1);
pos_y=cumtrapz(vel_y);
figure 
plot(vel_y)
title('vel_y')

%vel_z=cumtrapz(utime_imu,acc_mean_z1);
%pos_z=cumtrapz(utime_imu,vel_z);

dist_acc_x=[0 diff(pos_x)];
dist_acc_y=[0 diff(pos_y)];
%dist_acc_z_full=[0 diff(pos_z)];


[relcoord_x,relcoord_xy]=pol2cart(mag_yawangle_corrected,dist_acc_x);
abscoord_x=cumsum(relcoord_x);
abscoord_xy=cumsum(relcoord_xy);

%plot the grapth of gps data and  imu data
utm_x2 = mean(utm_x);
utm_y2 = mean(utm_y);
utm_xf = utm_x
figure 
plot(utm_x2,utm_y2)
title('Avg position')
utm_x1=utm_x(1,:)-utm_x(1,1);
utm_y1=utm_y(1,:)-utm_y(1,1);
figure
scatter(utm_x1,utm_y1)

hold on
plot(-abscoord_x,abscoord_xy)

title('gps data and imu data')
legend('GPS DATA','IMU DTA')

%5.1 comparision

YDD=acc_mean_y1;
w=gyro_z;
XD=vel_x;
WXD=w.*XD;

%WXD1 = cross(w,XD);
difference=mean(YDD-WXD)

% 5.3 comparision
% The abscoord_x is obtained in global frame while utm value in Y is
% obtained in gps global frame.
%Thus their absolte positions from car are relatively unknown.

Xc=mean(abscoord_x)-mean(utm_y1)

abs_diff=[0 diff(w)];
xc_t=(acc_mean_y1-(WXD));  % same as difference in part 1
for z=1:51210
    xc_1(z)=xc_t(z)/abs_diff(z);
end

leng=isinf(xc_t);
for zt=1:length(leng)
if leng(1,zt)==1
   xc_t(1,zt)=0; 
    
end
end
xc_1=mean(xc_t)