javaaddpath '/home/ujasmandavia/Desktop/lcm.jar'
javaaddpath '/home/ujasmandavia/Desktop/my_types.jar'

log_file = lcm.logging.Log('/home/ujasmandavia/Desktop/lcm-log-0226/lcmlog-2018-02-26.04', 'r');

i=1;
j=1;

while true
    try
        ev = log_file.readNext();
        disp('Hey')
        
        if strcmp(ev.channel, 'GPSData')
            disp('Hey1')
            gpsinfo = gpslcm.gps_lcm(ev.data);
            if gpsinfo.utm_x ~= 0
                disp('2')
                utm_x(i) = gpsinfo.utm_x;
                utm_y(i) = gpsinfo.utm_y;
                lat(i) = gpsinfo.latitude;
                long(i) = gpsinfo.longitude;
                alt(i) = gpsinfo.altitude;
                utime_gps(i) = gpsinfo.time;
                i = i + 1;
            end
        end
        
        if strcmp(ev.channel, 'IMUData')
            disp('3')
            imuinfo = imulcm.imu_lcm(ev.data);
            if imuinfo.mag_x ~= 0
                yaw(j) = imuinfo.yaw;
                mag_x(j) = imuinfo.mag_x;
                mag_y(j) = imuinfo.mag_y;
                mag_z(j) = imuinfo.mag_z;
                acc_x(j) = imuinfo.accel_x;
                acc_y(j) = imuinfo.accel_y;
                acc_z(j) = imuinfo.accel_z;
                gyro_x(j) = imuinfo.gyro_x;
                gyro_y(j) = imuinfo.gyro_y;
                utime_imu(i) = imuinfo.time;
                j = j + 1;
            end
        end
        
    catch err
        break;
    end
    i = i+1;
    j = j+1;
end

utm_x = utm_x(1:i-1);
utm_y = utm_y(1:i-1);
lat = lat(1:i-1);
long = long(1:i-1);
alt = alt(1:i-1);

yaw = yaw(1:j-1);
mag_x = mag_x(1:j-1);
mag_y = mag_y(1:j-1);
mag_z = mag_z(1:j-1);
acc_x = acc_x(1:j-1);
acc_y = acc_y(1:j-1);
acc_z = acc_z(1:j-1);
gyro_x = gyro_x(1:j-1);
gyro_y = gyro_y(1:j-1);

maxmag_x = max(mag_x);
minmag_x = min(mag_x);
meanmag_x = mean(mag_x);

maxmag_y = max(mag_y);
minmag_y = min(mag_y);
meanmag_y = mean(mag_y);

alpha =(maxmag_x + minmag_x)/2;
beta = (maxmag_y + maxmag_y)/2;
