import sys
import time
import lcm
import serial
import numpy as np


from mylcm import exampleimu_t

port = serial.Serial('/dev/ttyUSB0', baudrate= 115200)
lcm = lcm.LCM()
packet = exampleimu_t() 
print('IMU is up and running!')

while True:
	line = port.readline()
	data = line.split(',')

	print ('Its fine untill this point')

	if data[0] == '$VNYMR':
		data[data == '']='0'
		packet.yaw = float(data[1])
		print(packet.yaw)
		packet.roll = float(data[2])
		packet.pitch = float(data[3])
		packet.mag_x = float(data[4])
		print(packet.mag_x)
		packet.mag_y = float(data[5])
		packet.mag_z = float(data[6])
		packet.acc_x = float(data[7])
		print(packet.acc_x)
		packet.acc_y = float(data[8])
		packet.acc_z = float(data[9])
		packet.gyro_x = float(data[10])
		packet.gyro_y = float(data[11])
		packet12 = data[12]
		packet12_split = packet12.split('*')


		lcm.publish('IMU',packet.encode())

pass
